#!/bin/sh

##
# This script is intended to run from your current working directory
# containing whatever code you're in the middle of
# developing/debugging/testing.
##

PWD=$(pwd)
docker_dir=/$(basename $PWD)
BITBUCKET_BUILD_NUMBER=99
BITBUCKET_CLONE_DIR=$docker_dir
BITBUCKET_BRANCH=$(git rev-parse --abbrev-ref HEAD)
branch=$(echo ${BITBUCKET_BRANCH} | cut -f2 -d-)

##
# The following volume mappings are created between your local system
# and the docker container:
#
# ${PWD}:${docker_dir}
# Whatever your current working directory is, is mapped to /<whatever>
# Example:
#   PWD=/home/joe/some/long/directory/working_here
#   docker_dir=/working_here
#
#
# ${HOME}/.ssh:/root/.ssh
# Maps: ~/.ssh to /root/.ssh
#
# ${HOME}/.jfrog:/root/.jfrog 
# Maps ~/.jfrog to /root/.jfrog so the JFrog CLI will work from within
# in the docker container

# --workdir=${docker_dir}
# --workdir is the default PWD when you run docker and land in the container.
##

##
# The following is useful if you need/want to test running daemons
# and/or anything that requires systemd Add the following line to list
# below as the second line after the "docker run" line:
#
#  --cap-add=SYS_ADMIN \
##

docker run -it \
       --volume=${PWD}:${docker_dir} \
       --volume=${HOME}/.ssh:/root/.ssh \
       --volume=${HOME}/.jfrog:/root/.jfrog \
       --workdir=${docker_dir} \
       --memory=4g \
       --memory-swap=4g \
       --memory-swappiness=0 \
       --entrypoint=/bin/bash \
       -e ARTIFACTORY_URL=https://etq.jfrog.io/artifactory/ \
       -e ARTIFACTORY_USER=<USERNAME>@etq.com \
       -e ARTIFACTORY_API_KEY=<YOUR ARTIFACTORY API KEY> \
       -e BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER} \
       -e BITBUCKET_BRANCH=${BITBUCKET_BRANCH} \
       -e BITBUCKET_CLONE_DIR=${BITBUCKET_CLONE_DIR} \
       -e branch=${branch} \
       amazonlinux

##
# List of containers usefult for testing/debugging various things.
# Simply move one of the following containers to the last line of the
# script and make it the "active" container.
##
#       docker.bintray.io/jfrog/jfrog-cli-go:latest
#       atlassian/default-image:2
#       python:3.8.5-slim
#       pllsaph/seek-and-deploy:2020.12.3

###
# The following settings are useful when debugging terraform code and
# deployments.  These are the variables currently used by the
# deployment-artifacts pipeline to actually test deploying all of our
# code automatically. Just move the below block of settings to just
# underneath the last "-e branch" line prior to the list of containers
#
# Please note: To actually use the below settings you need to know the
# MFA secret, which is presented to you by default as a QR code in
# AWS.  You need to click the "Show secret key" link when setting up
# your MFA device and copy that string into the variable
# BBUSER_MFA_KEY below.  You will likely need to delete your existing
# MFA devide in AWS and create a new one so you can get to the secret.
##
       # -e env_name="do${branch}bn${BITBUCKET_BUILD_NUMBER}" \
       # -e file_name="${BITBUCKET_CLONE_DIR}/tests/CONFIG.txt.test" \
       # -e tmpCONFIG=/tmp/deployment-artifacts/CONFIG.txt \
       # -e AWS_CLI_PROFILE=etq-development \
       # -e DEV_AWS_ACCESS_KEY_ID="<YOUR ACCESS KEY ID>" \
       # -e DEV_AWS_SECRET_ACCESS_KEY="<YOUR ACCESS KEY SECRET" \
       # -e BBUSER_MFA_KEY=<YOUR MFA TOKEN SECRET>' \
       # -e BBUSER_MFA_ARN='<YOUR MFA ARN>' \
